#  Helm Chart Human–Computer interaction

Continuous deployement for the project [celery-prometheus-exporter](https://gitlab.com/bodzen/krakeno/forked-celery-prometheus-exporter)

## Requirements

- helm 3
- kubectl >= v1.16.3
