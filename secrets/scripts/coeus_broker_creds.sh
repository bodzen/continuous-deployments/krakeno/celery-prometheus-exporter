#!/bin/bash
# Create secret containing Coeus-Broker API Key

set -e
source $OUTPUT_FUNC

export SECRET_NAME='coeus-broker-url'
MANIFEST_FILE="${MANIFEST_PATH}/coeus-broker_creds.yaml"

if secret_already_exist
then
	print_secret_already_exist
else
	print_missing_secret
	/bin/sed -i 's|COEUS_BROKER_CREDS_B64|'"$COEUS_BROKER_CREDS_B64"'|g' $MANIFEST_FILE
	unset COEUS_BROKER_CREDS_B64
	kubectl -n $GIVEN_NAMESPACE apply -f $MANIFEST_FILE
	rm -f $MANIFEST_FILE
fi
