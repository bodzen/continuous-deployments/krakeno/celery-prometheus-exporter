#!/bin/bash
# Create secret containing Celery prometheus exporter
# project's sepecific Sentry URI

set -e
source $OUTPUT_FUNC

export SECRET_NAME='celery-prometheus-exporter-sentry-uri'
MANIFEST_FILE="${MANIFEST_PATH}/sentry_uri.yaml"

if secret_already_exist
then
	print_secret_already_exist
else
	print_missing_secret
	/bin/sed -i 's|SENTRY_URI_B64|'"$SENTRY_URI_B64"'|g' $MANIFEST_FILE
	unset SENTRY_URI_B64
	kubectl -n $GIVEN_NAMESPACE apply -f $MANIFEST_FILE
	rm -f $MANIFEST_FILE
fi
