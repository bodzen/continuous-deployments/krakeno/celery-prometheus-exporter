#!/bin/bash

print_secret_already_exist() {
	echo -e "[${GREEN}+${NC}] ${BLUE}${SECRET_NAME}${NC} already exist in namespace ${BLUE}${GIVEN_NAMESPACE}${NC}"
}

print_missing_secret() {
	echo -e "[${RED}-${NC}] ${BLUE}${SECRET_NAME}${NC} does not exist${NC}"
}

print_secret_creation_success() {
	echo -e "[${GREEN}+${NC}] ${BLUE}${SECRET_NAME}${NC} already exist in namespace ${BLUE}${GIVEN_NAMESPACE}${NC}"
}
