#!/bin/bash

export MANIFEST_PATH='./secrets/manifests/'
export SCRIPTS_PATH='./secrets/scripts'
export OUTPUT_FUNC="${SCRIPTS_PATH}/output_func.sh"

export BLUE='\033[34m'
export YELLOW='\033[33m'
export RED='\033[91m'
export GREEN='\033[92m'
export NC='\033[39m'

start_secret_generation() {
	echo -e "[${GREEN}+${NC}] Starting to execute ${YELLOW}$1${NC}"
	bash $SCRIPTS_PATH/$1
}

function secret_already_exist() {
	kubectl -n $GIVEN_NAMESPACE get secrets $SECRET_NAME 2>&- >/dev/null
	return $?
}
export -f secret_already_exist

SECRET_SCRIPTS=("coeus_broker_creds.sh sentry_uri.sh")

for script in ${SECRET_SCRIPTS[*]}; do
	start_secret_generation $script
done
